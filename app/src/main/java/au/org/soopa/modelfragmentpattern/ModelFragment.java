package au.org.soopa.modelfragmentpattern;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ModelFragment extends Fragment {

    public interface AccessModel {
        ArrayList<String> AccessModel_GetData();
        void AccessModel_Refresh();
    }

    private ArrayList<String> mData = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        refreshData();
    }

    public void refreshData() {
        // refresh model data on different thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                // get data
                mData.clear();
                mData.add("1");
                mData.add("2");
                mData.add("3");
                // send global event
                EventBus.getDefault().post(new onRefreshFinished());


            }
        }).start();
    }

    public ArrayList<String> getData() {
        // return current data
        return mData;
    }




    // EVENT BUS CLASSES
    public static class onRefreshFinished {

    }






}
