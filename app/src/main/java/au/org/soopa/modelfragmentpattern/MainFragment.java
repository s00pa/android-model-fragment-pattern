package au.org.soopa.modelfragmentpattern;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.greenrobot.event.EventBus;

public class MainFragment extends Fragment {

    private MainFragmentContract mContract;
    private ModelFragment.AccessModel mAccessModel;


    public interface MainFragmentContract {
        // put contract methods for activity to implement
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAccessModel.AccessModel_GetData(); // use data where it is needed

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);



        try {
            mContract = (MainFragmentContract) getActivity();
            mAccessModel = (ModelFragment.AccessModel) getActivity();
        }
        catch (ClassCastException e) {
            throw new ClassCastException("activity must implement MainFragmentContract and AccessModel");
        }

        EventBus.getDefault().register(this);

    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    // GLOBAL EVENT BUS
    @SuppressWarnings("unused")
    public void onEvent(ModelFragment.onRefreshFinished result) {
        mAccessModel.AccessModel_GetData(); // use data where it is needed
    }


}
