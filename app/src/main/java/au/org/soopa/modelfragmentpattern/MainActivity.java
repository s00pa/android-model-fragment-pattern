package au.org.soopa.modelfragmentpattern;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements ModelFragment.AccessModel, MainFragment.MainFragmentContract {

    public static final String TAG_MODEL_FRAGMENT = "model_fragment";
    public static final String TAG_MAIN_FRAGMENT = "main_fragment";

    ModelFragment mModelFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mModelFragment = (ModelFragment) getSupportFragmentManager().findFragmentByTag(TAG_MODEL_FRAGMENT);

        if (mModelFragment == null) {
            mModelFragment = new ModelFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(mModelFragment, TAG_MODEL_FRAGMENT)
                    .commit();
        }


        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(TAG_MAIN_FRAGMENT);

        if (mainFragment == null) {
            // Note: usually would instantiate fragment with newInstance factory
            mainFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mainFragment, TAG_MAIN_FRAGMENT)
                    .commit();
        }
    }

    @Override
    public ArrayList<String> AccessModel_GetData() {
        return mModelFragment.getData();
    }

    @Override
    public void AccessModel_Refresh() {
        mModelFragment.refreshData();
    }



}
